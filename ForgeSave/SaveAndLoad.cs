﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ernegien.HaloOnline.Utilities.Imports.Structs;
using Ernegien.HaloOnline.Utilities.Imports.Types;
using Ernegien.HaloOnline.Utilities.IO;
using Ernegien.HaloOnline.Utilities;
using System.Threading;
namespace ForgeSave
{
    public partial class SaveAndLoad : DevExpress.XtraEditors.XtraForm
    {
        string modDir = DarkPluginLib.DarkSettings.HaloOnlineFolder + @"mods\forge\";
        const int PROCESS_VM_WRITE = 0x0020;
        const int PROCESS_VM_OPERATION = 0x0008;
        const int PROCESS_WM_READ = 0x0010;

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(int hProcess, int lpBaseAddress,
          byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesWritten);
        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess,
          int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        public byte[] ReadMemory(int handle, int address, int len)
        {
            int bytesRead = 0;
            byte[] buffer = new byte[len];
            ReadProcessMemory(handle, address, buffer, buffer.Length, ref bytesRead);
            return buffer;
        }
        public SaveAndLoad()
        {
            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.Skins.SkinManager.EnableMdiFormSkins();
            DevExpress.LookAndFeel.LookAndFeelHelper.ForceDefaultLookAndFeelChanged();
            InitializeComponent();
        }


        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (DarkPluginLib.DarkSettings.HaloIsRunning)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Sorry, you can't load forge maps when Halo is running. (Coming soon)");
                ConsoleLog("Sorry, you can't load forge maps when Halo is running. (Coming soon)");
                return;
            }
            if (listVariants.Items.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You don't have any variants to load yet.. Try making some of your own!");
                ConsoleLog("You don't have any variants to load yet.. Try making some of your own!");
                return;
            }
            if (listVariants.SelectedValue.ToString() == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("You need to select a Forge Variant to load first!");
                ConsoleLog("You need to select a Forge Variant to load first!");
                return;
            }
            else
            {
                string loadFile = modDir + listVariants.SelectedValue.ToString();
                if (File.Exists(loadFile))
                {
                    ConsoleLog("Loading " + loadFile + "...");
                    ConsoleLog("Patching " + DarkPluginLib.DarkSettings.HaloOnlineFolder + @"preferences.dat...");
                    using (Stream stream = new FileStream(DarkPluginLib.DarkSettings.HaloOnlineFolder + @"preferences.dat", FileMode.OpenOrCreate))
                    {
                        byte[] loadPatch = File.ReadAllBytes(loadFile);
                        stream.Seek(2032, SeekOrigin.Begin);  //7F0
                        stream.Write(loadPatch, 0, loadPatch.Length);
                        ConsoleLog("Wrote " + loadPatch.Length + " bytes to 0x7F0");
                        stream.Seek(60056, SeekOrigin.Begin);  //EA98
                        stream.Write(loadPatch, 0, loadPatch.Length);
                        ConsoleLog("Wrote " + loadPatch.Length + " bytes to 0xEA98");
                    }
                    ConsoleLog("Done! Load up Halo Online and play away!");
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("That forge variant doesn't exist.. Did you just delete it?");
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            if (!DarkPluginLib.DarkSettings.HaloIsRunning)
            {
                ConsoleLog("Halo must be running in order to save a forge variant");
                DevExpress.XtraEditors.XtraMessageBox.Show("Halo must be running in order to save a forge variant");
                return;
            }
            
            if (txtName.Text == "")
            {
                ConsoleLog("You need to include your name to save a forge variant.");
                DevExpress.XtraEditors.XtraMessageBox.Show("You need to include your name to save a forge variant.");
                return;
            }
            if (txtMap.Text == "")
            {
                ConsoleLog("You need to include a map name for your variant.");
                DevExpress.XtraEditors.XtraMessageBox.Show("You need to include a map name for your variant.");
                return;
            }
            try
            {

                GameProcess game = new GameProcess("eldorado");
                ConsoleLog("Found game process.");

                uint tls = game.TlsAddress;
                ConsoleLog(string.Format("TLS Address: 0x{0}", Convert.ToString(tls, 16).PadLeft(8, '0')));

                uint gameEngineGlobals = game.Memory.ReadUInt32(game.TlsAddress + 0x48);
                uint gameGlobals = game.Memory.ReadUInt32(game.TlsAddress + 0x3C);

                ConsoleLog(string.Format("gameEngineGlobals 0x{0}", Convert.ToString(gameEngineGlobals, 16).PadLeft(8, '0')));
                ConsoleLog(string.Format("gameGlobals 0x{0}", Convert.ToString(gameGlobals, 16).PadLeft(8, '0')));

                uint baseMapVarient = gameEngineGlobals + 0x78; // GET FROM TLS

                uint variantDataIndex = gameGlobals + 0x33C;

                //int preferencesDatStatic = 0x22C0130;

                byte[] MapName = new byte[32];
                byte[] MapAuthor = new byte[32];

                MapName = ReadMemory((int)game.ProcessHandle, (int)baseMapVarient + 0x8, 32);

                string mapName = Encoding.Unicode.GetString(MapName);
                mapName = Encoding.ASCII.GetString(
                                                Encoding.Convert(
                                                    Encoding.UTF8,
                                                    Encoding.GetEncoding(
                                                        Encoding.ASCII.EncodingName,
                                                        new EncoderReplacementFallback(string.Empty),
                                                        new DecoderExceptionFallback()
                                                        ),
                                                    Encoding.UTF8.GetBytes(mapName)
                                                )
                                            ).Replace("\0", "");
                ConsoleLog("Map name: " + mapName);

                if (txtMap.Text == "")
                {
                    txtMap.Text = mapName;
                }

                MapAuthor = ReadMemory((int)game.ProcessHandle, (int)baseMapVarient + 0xA8, 32);

                string mapAuthor = Encoding.Unicode.GetString(MapAuthor);
                mapAuthor = Encoding.ASCII.GetString(MapAuthor).Replace("\0", "");

                ConsoleLog("Map Author Old: " + mapAuthor);

                int bytesWritten = 0;
                byte[] newName = new Byte[32];

                newName = Encoding.ASCII.GetBytes(txtName.Text);

                byte[] namePadding = new Byte[32];

                newName.CopyTo(namePadding, 0);

                ConsoleLog("Patching Forge Author to " + txtName.Text + "...");

                WriteProcessMemory((int)game.ProcessHandle, (int)baseMapVarient + 0xA8, namePadding, namePadding.Length, ref bytesWritten);


                byte[] newMapName = new Byte[32];

                newMapName = Encoding.Unicode.GetBytes(txtMap.Text+"\0");

                byte[] nameMapPadding = new Byte[32];

                newMapName.CopyTo(nameMapPadding, 0);


                ConsoleLog("Patching Forge Map Name to " + txtMap.Text + "...");

                WriteProcessMemory((int)game.ProcessHandle, (int)baseMapVarient + 0x8, newMapName, newMapName.Length, ref bytesWritten);

                uint beginningOfMap = baseMapVarient;

                uint endOfMap = beginningOfMap + 0xE090;

                ConsoleLog("Map begining index: 0x" + beginningOfMap.ToString("X"));
                ConsoleLog("End of map: 0x" + endOfMap.ToString("X"));

                ConsoleLog("Writing map varient to " + modDir);

                uint length = endOfMap - beginningOfMap;
                byte[] MapBytes = new byte[length];


                MapBytes = ReadMemory((int)game.ProcessHandle, (int)beginningOfMap, (int)length);

                //Directory.CreateDirectory("mods/");
                String date = DateTime.Now.ToString("dd.MM.yyyy hh-mm-ss");
                var filename = txtMap.Text + " ("+txtName.Text + ") - " + date;
                File.WriteAllBytes(modDir + filename + ".frg", MapBytes);

                ConsoleLog("Wrote out " + (length) + " bytes to " + modDir + filename + ".frg");
                refreshVariants();


                byte[] newNameUnicode = Encoding.Unicode.GetBytes("DTK Saved! " + DateTime.Now.ToString("t") + "\0\0\0\0");
                WriteProcessMemory((int)game.ProcessHandle, (int)variantDataIndex + 0x34, newNameUnicode, newNameUnicode.Length, ref bytesWritten);

            }
            catch (Exception ex)
            {
                ConsoleLog("Something bad happened.. Is Halo Online running? Are you in game?");
                ConsoleLog(ex.Message);
            }

        }

        public void ConsoleLog(string text)
        {
            listConsole.Items.Add(text);
            listConsole.MakeItemVisible(listConsole.Items.Count);

            DarkPluginLib.DarkLog.WriteLine(text);
        }

        private void Main_Load(object sender, EventArgs e)
        {
            ForgeSavePlugin.ForgeSave.runningForm = true;
            Directory.CreateDirectory(modDir);

            refreshVariants();
            running = true;
            Thread mapVariant = new Thread(DisplayVariant);
            mapVariant.Start();
        }
        bool running = false;
        public void DisplayVariant()
        {
            while (running)
            {
                if (DarkPluginLib.DarkSettings.HaloIsRunning)
                {
                    try
                    {
                        GameProcess game = new GameProcess("eldorado");
                        uint tls = game.TlsAddress;
                        uint gameEngineGlobals = game.Memory.ReadUInt32(game.TlsAddress + 0x48);
                        uint gameGlobals = game.Memory.ReadUInt32(game.TlsAddress + 0x3C);

                       
                        uint baseMapVarient = gameEngineGlobals + 0x78; // GET FROM TLS

                        uint variantDataIndex = gameGlobals + 0x33C;

                        int bytesWritten = 0;

                        byte[] MapAuthor = new Byte[32];

                        MapAuthor = ReadMemory((int)game.ProcessHandle, (int)baseMapVarient + 0xA8, 32);

                        string mapAuthor = Encoding.Unicode.GetString(MapAuthor);
                        mapAuthor = Encoding.ASCII.GetString(MapAuthor).Replace("\0", "").Trim();

                        //For some reason even though we have a custom author it still always loads "Bungie"
                        //Let's fix that...
                        if (mapAuthor == "Bungie")
                        {
                            MapAuthor = ReadMemory((int)game.ProcessHandle, (int)0x022C09C8, 32);
                            mapAuthor = Encoding.Unicode.GetString(MapAuthor);
                            mapAuthor = Encoding.ASCII.GetString(MapAuthor).Replace("\0", "");

                            byte[] newName = new byte[32];
                            newName = Encoding.ASCII.GetBytes(mapAuthor);

                            byte[] namePadding = new Byte[32];

                            newName.CopyTo(namePadding, 0);

                            ConsoleLog("Patching Forge Author to " + mapAuthor + "...");

                            WriteProcessMemory((int)game.ProcessHandle, (int)baseMapVarient + 0xA8, namePadding, namePadding.Length, ref bytesWritten);
                        }

                        byte[] newNameUnicode = Encoding.Unicode.GetBytes("Map Variant By " + mapAuthor + "\0\0\0\0");

                      //  Array.Clear(newNameUnicode, newNameUnicode.Length, 32);
                        
                        WriteProcessMemory((int)game.ProcessHandle, (int)variantDataIndex + 0x34, newNameUnicode, newNameUnicode.Length, ref bytesWritten);
                        Thread.Sleep(5000);
                    }
                    catch (Exception)
                    {

                    }
                }
            }

        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            refreshVariants();
        }

        public void refreshVariants()
        {
            listVariants.Items.Clear();
            DirectoryInfo modsForge = new DirectoryInfo(modDir);
            foreach (var file in modsForge.GetFiles("*.frg"))
            {
                listVariants.Items.Add(file);
            }
        }

        private void SaveAndLoad_FormClosing(object sender, FormClosingEventArgs e)
        {
            running = false;
            ForgeSavePlugin.ForgeSave.runningForm = false;
        }

        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            Process.Start("https://halowiki.llf.to/index.php?title=ForgeVariants", "");
        }
    }
}
