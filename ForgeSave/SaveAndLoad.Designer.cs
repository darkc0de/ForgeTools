﻿namespace ForgeSave
{
    partial class SaveAndLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaveAndLoad));
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.listConsole = new DevExpress.XtraEditors.ListBoxControl();
            this.btnLoadVariant = new DevExpress.XtraEditors.SimpleButton();
            this.btnSaveVariant = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.listVariants = new DevExpress.XtraEditors.ListBoxControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtMap = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.listConsole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.listVariants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMap.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.defaultLookAndFeel1.LookAndFeel.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.False;
            // 
            // listConsole
            // 
            this.listConsole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listConsole.Location = new System.Drawing.Point(2, 21);
            this.listConsole.Name = "listConsole";
            this.listConsole.Size = new System.Drawing.Size(537, 131);
            this.listConsole.TabIndex = 5;
            // 
            // btnLoadVariant
            // 
            this.btnLoadVariant.Location = new System.Drawing.Point(210, 44);
            this.btnLoadVariant.Name = "btnLoadVariant";
            this.btnLoadVariant.Size = new System.Drawing.Size(75, 23);
            this.btnLoadVariant.TabIndex = 6;
            this.btnLoadVariant.Text = "Load Variant";
            this.btnLoadVariant.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // btnSaveVariant
            // 
            this.btnSaveVariant.Location = new System.Drawing.Point(212, 109);
            this.btnSaveVariant.Name = "btnSaveVariant";
            this.btnSaveVariant.Size = new System.Drawing.Size(75, 23);
            this.btnSaveVariant.TabIndex = 7;
            this.btnSaveVariant.Text = "Save Variant";
            this.btnSaveVariant.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.listConsole);
            this.groupControl1.Location = new System.Drawing.Point(12, 242);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(541, 154);
            this.groupControl1.TabIndex = 8;
            this.groupControl1.Text = "Forge Console";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.labelControl4);
            this.groupControl2.Controls.Add(this.txtMap);
            this.groupControl2.Controls.Add(this.labelControl2);
            this.groupControl2.Controls.Add(this.labelControl1);
            this.groupControl2.Controls.Add(this.txtName);
            this.groupControl2.Controls.Add(this.btnSaveVariant);
            this.groupControl2.Location = new System.Drawing.Point(254, 12);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(299, 143);
            this.groupControl2.TabIndex = 9;
            this.groupControl2.Text = "Save";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(14, 24);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(260, 52);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = "Once you\'re finishing making changes in game you can\r\nnow save your forge variant" +
    " to share with friends.\r\n\r\nPut in a name and then type \"Save Variant\"";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(15, 114);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(51, 13);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Your name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(73, 111);
            this.txtName.Name = "txtName";
            this.txtName.Properties.LookAndFeel.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.False;
            this.txtName.Properties.MaxLength = 15;
            this.txtName.Size = new System.Drawing.Size(133, 20);
            this.txtName.TabIndex = 9;
            // 
            // listVariants
            // 
            this.listVariants.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listVariants.Location = new System.Drawing.Point(2, 21);
            this.listVariants.Name = "listVariants";
            this.listVariants.Size = new System.Drawing.Size(232, 201);
            this.listVariants.TabIndex = 10;
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.listVariants);
            this.groupControl3.Location = new System.Drawing.Point(12, 12);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(236, 224);
            this.groupControl3.TabIndex = 11;
            this.groupControl3.Text = "Available Forge Variants";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(112, 44);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(92, 23);
            this.btnRefresh.TabIndex = 8;
            this.btnRefresh.Text = "Refresh Variants";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.labelControl3);
            this.groupControl4.Controls.Add(this.btnRefresh);
            this.groupControl4.Controls.Add(this.btnLoadVariant);
            this.groupControl4.Location = new System.Drawing.Point(254, 161);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(299, 75);
            this.groupControl4.TabIndex = 12;
            this.groupControl4.Text = "Load";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(14, 24);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(197, 13);
            this.labelControl3.TabIndex = 9;
            this.labelControl3.Text = "Forge mods are loaded from mods/forge/";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(23, 91);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(43, 13);
            this.labelControl4.TabIndex = 13;
            this.labelControl4.Text = "Map Title";
            // 
            // txtMap
            // 
            this.txtMap.Location = new System.Drawing.Point(73, 88);
            this.txtMap.Name = "txtMap";
            this.txtMap.Properties.LookAndFeel.TouchUIMode = DevExpress.LookAndFeel.TouchUIMode.False;
            this.txtMap.Properties.MaxLength = 32;
            this.txtMap.Size = new System.Drawing.Size(133, 20);
            this.txtMap.TabIndex = 12;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(258, 408);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(212, 13);
            this.labelControl5.TabIndex = 13;
            this.labelControl5.Text = "Share your Forge Variant for others to play!";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(476, 403);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 14;
            this.simpleButton1.Text = "Share";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // SaveAndLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 436);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SaveAndLoad";
            this.Text = "Forge Save Game Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SaveAndLoad_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listConsole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.listVariants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMap.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraEditors.ListBoxControl listConsole;
        private DevExpress.XtraEditors.SimpleButton btnLoadVariant;
        private DevExpress.XtraEditors.SimpleButton btnSaveVariant;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.ListBoxControl listVariants;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtMap;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
    }
}

